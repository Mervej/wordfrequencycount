const http = require('http');
const https = require('https');
const request = require('request');
const express = require('express');

const hostname = '127.0.0.1';
const port = '5555';

const app = express();

app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

	app.get('/', function(req,res){
		requestdata(request,function(response){
			res.send(response.body);  	
	  		res.end();
		});
	})

app.listen(port, () =>{
	console.log('server running');
})


var url = 'https://terriblytinytales.com/test.txt';

var requestdata = function(request, cb){

	var headersOpt = {  
    		"content-type": "application/text",
	};

	request({
		method: 'get',
		url: url,
		headers: headersOpt
	}, function(error, response){
		if(error){
			console.log('error in post request'+error);
			return;
		}
		console.log('response from inside request is '+ response.statusCode);
		cb(response);
	});	
}
