import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class RestApiService {

  constructor(private http: HttpClient) { }

  getData(url: string) {
	    return new Promise((resolve, reject) => {
			this.http.get(url, {responseType: 'text'})
				.subscribe(data => {
		      		resolve(data);	
		    	}
		    	,error => {
		    		reject(error);
		    	});
		});
	}


}
