import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from '../../rest-api.service';

@Component({
  selector: 'app-frequencycount',
  templateUrl: './frequencycount.page.html',
  styleUrls: ['./frequencycount.page.scss'],
})
export class FrequencycountPage implements OnInit {

	url = "http://localhost:5555";	
  	numberOfWords = null;
  	sortable = [];
  	constructor(private activatedRoute: ActivatedRoute,private restProvider: RestApiService) { }

 	ngOnInit() {
   		this.numberOfWords = this.activatedRoute.snapshot.paramMap.get('numberOfWords');
   		this.getData(this.numberOfWords);
 	}

 	getData(limit: any){
  		this.restProvider.getData(this.url)
	      	.then(data => {
	      		this.CalculateTopWords(data, limit);
	    	})
	      	.catch ((error)=>{
	      		console.log(error.message);
	    	}); 	
 	};


 	CalculateTopWords(textData: string, limit: any){
		var wordCounts = { };
		var words = textData.split(/\b/);
		for(var i = 0; i < words.length; i++){
			var isValid = /^[a-z]+$/.test(words[i].toLowerCase());
			if(isValid)
			{
			    wordCounts[words[i].toLowerCase()] = (wordCounts[words[i].toLowerCase()] || 0) + 1;
			}
		}
		
		for (var item in wordCounts) {
		    this.sortable.push([item, wordCounts[item]]);
		}

		this.sortable.sort(function(a, b) {
		    return a[1] - b[1];
		});

		this.sortable = this.sortable.slice(0).slice(-limit);
    }
}
