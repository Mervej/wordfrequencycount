import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrequencycountPage } from './frequencycount.page';

describe('FrequencycountPage', () => {
  let component: FrequencycountPage;
  let fixture: ComponentFixture<FrequencycountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrequencycountPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrequencycountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
