import { Component } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  	constructor(private activatedRoute: ActivatedRoute, private router: Router, private alertCtrl: AlertController) {
  	}

  	validateInputNumber(limit: any){
  		if(limit!=null)
			this.router.navigate(['/frequencycount/',limit]);
		else{
			this.presentAlert();
		}
	}

	async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      message: 'Please Enter a valid number',
      buttons: ['OK']
    });

    await alert.present();
  }
}
